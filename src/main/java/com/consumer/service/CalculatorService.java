package com.consumer.service;

import com.consumer.model.CalculationRequest;
import com.consumer.model.Output;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

@Service
public class CalculatorService {

    private final String BASE_URL = "http://localhost:8081/api/v1/calculator";
    private final RestTemplate restTemplate;

    public CalculatorService() {
        this.restTemplate = new RestTemplate();
    }

    public Output add(int number1, int number2) {
        return calculate("/add", number1, number2);
    }

    private Output calculate(String endpoint, int number1, int number2) {
        CalculationRequest request = new CalculationRequest(number1, number2);
        try {
            ResponseEntity<Output> responseEntity = restTemplate.postForEntity(
                    BASE_URL + endpoint,
                    request,
                    Output.class
            );
            return responseEntity.getBody();
        } catch (HttpClientErrorException e) {
            throw new RuntimeException("API call failed with HTTP status: " + e.getStatusCode());
        } catch (Exception e) {
            throw new RuntimeException("API call failed: " + e.getMessage());
        }
    }

}
